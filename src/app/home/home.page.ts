
import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

import { AuthConstants } from '../config/auth-constants';
import { StorageService } from '../services/storage.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  menuItems = [
    {
      title: 'Home',
      icon: 'home',
      path: '/home'
    },
    {
      title: 'Account',
      icon: 'person-circle',
      path: '/home/account'
    },

    {
      title: 'Settings',
      icon: 'settings',
      path: '/home/settings'
    },
    {
      title: 'Contact',
      icon: 'phone-portrait',
      path: '/home/contact'
    },
    {
      title: 'About',
      icon: 'information',
      path: '/home/about'
    }
  ];

  title = 'Home';
  userData = null;
  constructor(
    private menuCtrl: MenuController,
    private plt: Platform,
    private router: Router,
    private authService: AuthService,
    public storageService: StorageService,

  ) { }

  ngOnInit() {
    const width = this.plt.width();
    this.toggleMenu(width);

    //this.storageService.get()
    this.storageService
      .get(AuthConstants.AUTH)
      .then(res => {
        if (res) {
          this.userData = res;
          console.log('homepage: , this.userData =', this.userData);
        } else {
          this.router.navigate(['login']);
          console.log('homepage: , go to login');
        }
      })
      .catch(err => {
        console.log('homepage: , err=', err);
      });

    let xtitle = this.router.url.split('/').pop();
    let myObj = this.menuItems.find(obj => obj.path === this.router.url);
    console.log('homepage: router.myObj', myObj);
    this.title = myObj.title;
  }

  @HostListener('window:resize', ['$event'])
  private onResize(event) {
    const newWidth = event.target.innerWidth;
    this.toggleMenu(newWidth);
  }


  toggleMenu(width) {
    if (width > 768) {
      this.menuCtrl.enable(false, 'myMenu');
    } else {
      this.menuCtrl.enable(true, 'myMenu');
    }
  }

  setTitle(item) {
    this.title = item.title;
    this.router.navigate([`${item.path}`]);

  }

  logout() {
    this.authService.logout();
  }
}
